Category = {
	
	init: function(){
		this.categories = {};
		this.flatIt(categories);
	},
		
	click: function(categoryId){
		var data = this.categories[categoryId];
		if(data){
			var html = new EJS({element: 'categoryDetail'}).render({data: data, cats: this.categories});
			$('#wrapper').html(html);
		}
	},
	
	save: function(categoryId){
		var data = {
			_id: categoryId,
			categoryName: $('#category-edit .category-name').val(),
			categorySlug: $('#category-edit .category-slug').val(),
			parentId: $('#category-edit .parent-id').val(),
			isActive: $('#category-edit .is-active').val()
		}; 
		$.ajax({
			url: Config.service + '/api/category/save',
			method: 'POST',
			data: data,
			success: function(ret){
				console.log(ret);
			}
		})
	},
	
	flatIt: function(cats){
		for(var i=0; i < cats.length; i++){
			this.categories[cats[i].id] = cats[i];
			if(cats[i].children){
				this.flatIt(cats[i].children);
			}
		};
	}
}

$(document).ready(function(){
	Category.init();
})