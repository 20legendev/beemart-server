package com.beemart.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.beemart.base.JSONUtils;
import com.beemart.model.Category;
import com.beemart.service.CategoryService;

@RestController
@RequestMapping(value="/admin")
public class AdminController {
	
	/**
	 * Returns all registered series
	 * @return
	 * @throws InterruptedException 
	 */
	
	@Autowired
	private CategoryService catSrv;
	
	@RequestMapping(value="/category", method=RequestMethod.GET)
	public ModelAndView categoryManager() throws InterruptedException {
		Category[] categories = this.catSrv.getAllCategory();
		ModelAndView model = new ModelAndView("admin/category");
		model.addObject("categories", categories);
		model.addObject("categoryString", JSONUtils.toJSONString(categories));
		return model;
	}
}
