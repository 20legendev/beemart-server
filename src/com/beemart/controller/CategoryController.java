package com.beemart.controller;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.beemart.model.Category;
import com.beemart.service.CategoryService;

@Controller
@RequestMapping("/api")
public class CategoryController {

	private CategoryService categoryService;

	@Autowired
	public CategoryController(CategoryService categoryService) {
		// TODO Auto-generated constructor stub
		this.categoryService = categoryService;
	}

	@RequestMapping(value = "/hello", method = RequestMethod.GET)
	@ResponseBody
	public String[] randomPerson() {
		String[] a = new String[2];
		a[0] = "kien";
		a[1] = "Nguyen";
		return a;
	}

	@RequestMapping(value = "/menu/home", method = RequestMethod.GET)
	@ResponseBody
	public Category[] getHomeMenu() {
		Category[] cat = categoryService.getAllCategory();
		return cat;
	}

	@RequestMapping(value = "/category/save", method = RequestMethod.POST)
	@ResponseBody
	/*
	public Category addCategory(Category category) {
		String id = categoryService.insertOrSave(category);
		return categoryService.getCategory(id);
	}
	*/
	public String addCategory(HttpServletRequest request){
		Enumeration<String> catName = request.getAttributeNames();
		return catName.nextElement();
	}

}
