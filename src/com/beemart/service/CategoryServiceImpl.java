package com.beemart.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.beemart.model.Category;
import com.beemart.service.CategoryService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

@Service
public class CategoryServiceImpl implements CategoryService{
	@Autowired
	private MongoOperations mongoOps;

	@Override
	public Category[] getAllCategory() {
		// TODO Auto-generated method stub
		List<Category> list = mongoOps.findAll(Category.class);
		return list.toArray(new Category[0]);
	}

	@Override
	public Category getCategory(String id) {
		Category cat = mongoOps.findById(id, Category.class);
		return cat;
	}

	@Override
	public String insertCategory(Category category) {
		mongoOps.insert(category);
		return category.getId();
	}

	@Override
	public void deleteCategory(String id) {
		Query query = new Query();
		Criteria criteria = new Criteria("_id").is(id);
		query.addCriteria(criteria);
		mongoOps.remove(query, Category.class);
	}

	@Override
	public String saveCategory(Category category) {
		// TODO Auto-generated method stub
		mongoOps.save(category);
		return category.getId();
	}

	@Override
	public String getAllCategoryString() {
		// TODO Auto-generated method stub
		Category[] cat = this.getAllCategory();
		ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
		String json;
		try {
			json = ow.writeValueAsString(cat);
			return json;
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "";
		}
	}

	@Override
	public String insertOrSave(Category category) {
		// TODO Auto-generated method stub
		String id = category.getId();
		Category cat = mongoOps.findById(id, Category.class);
		if(cat == null){
			Category parent = this.getCategory(category.getParentId());
			if(parent != null){
				List<Category> children = parent.getChildren(); 
				if(children == null){
					parent.setChildren(new ArrayList<Category>());
				}
				children.add(category);
				return this.saveCategory(category);
			}
			return this.insertCategory(category);
		}else{
			return this.saveCategory(category);
		}
	}
	
}
