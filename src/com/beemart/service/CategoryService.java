package com.beemart.service;

import com.beemart.model.Category;

public interface CategoryService {

	Category[] getAllCategory();
	
	String getAllCategoryString();

	Category getCategory(String id);

	String insertCategory(Category category);

	void deleteCategory(String id);
	
	String saveCategory(Category category);
	
	String insertOrSave(Category category);
}
